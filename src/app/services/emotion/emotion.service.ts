import { Injectable, Query } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { API_URL, TOKEN_KEY } from 'src/app/helpers/common/Constants';
import EmotionAnalytics from 'src/app/models/EmotionAnalytics';
import { QuestionEmotionMapper } from 'src/app/models/QuestionEmotionMapper';
import { UserService } from '../user/user.service';
import { Router } from '@angular/router';

@Injectable({
	providedIn: 'root'
})
export class EmotionService {
	static readonly API_URL = API_URL;
	static readonly TOKEN_KEY = TOKEN_KEY;

	allAnalytics: any;

	constructor(private httpClient: HttpClient, private userService: UserService, private router: Router) {}

	/**
	 * Get all emotion analytics from db.
	 */
	getEmotionAnalytics() {
		return this.httpClient
			.get<EmotionAnalytics[]>(`${EmotionService.API_URL}/analytics`, {
				headers: new HttpHeaders({
					Authorization: `Bearer ${this.userService.token}`
				})
			})
			.subscribe((response) => {
				this.allAnalytics = response;
			});
	}

	/**
	 * Get emotion analytics by interview.
	 * @param interviewId
	 */
	getEmotionAnalyticsByInterview(interviewId: number) {
		let body = new HttpParams().set('interviewId', interviewId.toString());

		return this.httpClient
			.post<EmotionAnalytics[]>(`${EmotionService.API_URL}/analyticsByInterview`, body, {
				headers: new HttpHeaders({
					Authorization: `Bearer ${this.userService.token}`
				})
			})
			.toPromise()
			.then((response) => {
				return response;
			})
			.catch((error) => {
				if (error.status === 401 || error.status === 403) {
					alert('You have been logged out! Please log in again!');
					this.userService.logout();
					this.router.navigate([ '/login' ]);
				}
				debugger;
				return undefined;
			});
	}

	/**
	 * Get emotion analytics by user.
	 * @param username 
	 */
	getEmotionAnalyticsByUser(username: string) {
		let body = new HttpParams().set('username', username);

		return this.httpClient
			.post<EmotionAnalytics[]>(`${EmotionService.API_URL}/analyticsByUser`, body, {
				headers: new HttpHeaders({
					Authorization: `Bearer ${this.userService.token}`
				})
			})
			.toPromise()
			.then((response) => {
				return response;
			})
			.catch((error) => {
				if (error.status === 401 || error.status === 403) {
					alert('You have been logged out! Please log in again!');
					this.userService.logout();
					this.router.navigate([ '/login' ]);
				}
				debugger;
				return undefined;
			});
	}

	/**
	 * Add emotion analytics to db.
	 * @param interviewId 
	 * @param username 
	 * @param feedback 
	 */
	addEmotionAnalytics(interviewId: number, username: string, feedback: QuestionEmotionMapper) {
		let body = new HttpParams()
			.set('interviewId', interviewId.toString())
			.set('username', username)
			.set('feedback', JSON.stringify(feedback));

		return this.httpClient
			.post(`${EmotionService.API_URL}/addEmotionAnalytics`, body, {
				headers: new HttpHeaders({
					Authorization: `Bearer ${this.userService.token}`
				})
			})
			.toPromise()
			.then((res: any) => {
				return true;
			})
			.catch((error) => {
				if (error.status === 401 || error.status === 403) {
					alert('You have been logged out! Please log in again!');
					this.userService.logout();
					this.router.navigate([ '/login' ]);
				}
				return false;
			});
	}
}
