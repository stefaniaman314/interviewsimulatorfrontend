import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { API_URL, TOKEN_KEY, MINUTE_IN_MILLISECONDS } from 'src/app/helpers/common/Constants';
import * as jwtDecode from 'jwt-decode';
import { Router } from '@angular/router';

@Injectable({
	providedIn: 'root'
})
export class UserService {
	static readonly API_URL = API_URL;
	static readonly TOKEN_KEY = TOKEN_KEY;

	constructor(private httpClient: HttpClient, private router: Router) {}

	set token(token: string) {
		localStorage.setItem(UserService.TOKEN_KEY, token);
	}

	get token() {
		return localStorage.getItem(UserService.TOKEN_KEY);
	}

	set email(email: string) {
		localStorage.setItem('email', email);
	}

	get email() {
		return localStorage.getItem('email');
	}

	set username(username: string) {
		localStorage.setItem('username', username);
	}

	get username() {
		return localStorage.getItem('username');
	}

	set logInStatus(status: string) {
		localStorage.setItem('logInStatus', status);
	}

	get logInStatus() {
		return localStorage.getItem('logInStatus');
	}

	/**
   * Login an existing user.
   * @param email 
   * @param password
   */
	login(email: string, password: string) {
		let body = new HttpParams().set('email', email).set('password', password);
		return this.httpClient
			.post(`${UserService.API_URL}/login`, body)
			.toPromise()
			.then((response: any) => {
				this.token = response.token;
				this.email = email;
				this.username = jwtDecode(this.token).username;
				this.logInStatus = 'true';
				return true;
			})
			.catch((err) => {
				this.logInStatus = 'false';
				return false;
			});
	}

	/**
	 * Log out a user.
	 */
	logout() {
		localStorage.removeItem(TOKEN_KEY);
		localStorage.removeItem('email');
		localStorage.removeItem('username');
		this.logInStatus = 'false';

		this.router.navigate([ '/' ]);
	}

	/**
   * Register a new user.
   * @param username 
   * @param password 
   * @param email 
   * @param securityQuestion 
   * @param securityQuestionAnswer 
   */
	register(
		username: string,
		password: string,
		email: string,
		securityQuestion: string,
		securityQuestionAnswer: string
	) {
		// If already logged in
		if (this.email === email) {
			return false;
		}

		let body = new HttpParams()
			.set('username', username)
			.set('password', password)
			.set('email', email)
			.set('securityQuestion', securityQuestion)
			.set('securityQuestionAnswer', securityQuestionAnswer);

		return this.httpClient
			.post(`${UserService.API_URL}/register`, body)
			.toPromise()
			.then((response: any) => {
				return true;
			})
			.catch((err) => {
				return false;
			});
	}

	/**
   * Recover the password of an existing user.
   * @param email 
   * @param securityQuestion 
   * @param securityQuestionAnswer 
   * @param newPassword 
   */
	recover(email: string, securityQuestion: string, securityQuestionAnswer: string, newPassword: string) {
		// If already logged in
		if (this.email === email) {
			return false;
		}

		let body = new HttpParams()
			.set('email', email)
			.set('securityQuestion', securityQuestion)
			.set('securityQuestionAnswer', securityQuestionAnswer)
			.set('newPassword', newPassword);

		return this.httpClient
			.post(`${UserService.API_URL}/recover`, body)
			.toPromise()
			.then((response: any) => {
				return true;
			})
			.catch((err) => {
				return false;
			});
	}
}
