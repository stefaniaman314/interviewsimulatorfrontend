import { Injectable } from '@angular/core';
import { Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { UserService } from '../user/user.service';

@Injectable({
	providedIn: 'root'
})
export class AuthGuardService {
	constructor(private router: Router, private userService: UserService) {}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		// if not logged in
		if (this.userService.logInStatus === 'false') {
			this.router.navigate([ '/login' ]);
			return false;
		}
		return true;
	}
}
