import { Injectable } from '@angular/core';
import { TOKEN_KEY, API_URL } from 'src/app/helpers/common/Constants';
import Question from 'src/app/models/Question';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserService } from '../user/user.service';
import { Router } from '@angular/router';

@Injectable({
	providedIn: 'root'
})
export class QuestionService {
	static readonly API_URL = API_URL;
	static readonly TOKEN_KEY = TOKEN_KEY;

	securityQuestions: Question[];
	quizQuestions: Question[];

	constructor(private httpClient: HttpClient, private userService: UserService, private router: Router) {}

	/**
	 * Retrieve all security questions from db for auth service.
	 */
	getSecurityQuestions() {
		return this.httpClient
			.get<Question[]>(`${QuestionService.API_URL}/securityQuestions`, {})
			.subscribe((response) => {
				this.securityQuestions = response;
			});
	}

	/**
	 * Retrieve all quiz questions from db for the interview.
	 */
	getQuizQuestions() {
		return this.httpClient
			.get<Question[]>(`${QuestionService.API_URL}/quizQuestions`, {
				headers: new HttpHeaders({
					Authorization: `Bearer ${this.userService.token}`
				})
			})
			.toPromise()
			.then((response) => {
				this.quizQuestions = response;
			})
			.catch((error) => {
				if (error.status === 401 || error.status === 403) {
					alert('You have been logged out! Please log in again!');
					this.userService.logout();
					this.router.navigate([ '/login' ]);
				}
			});
	}
}
