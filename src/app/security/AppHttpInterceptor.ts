import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, onErrorResumeNext } from 'rxjs';
import { UserService } from '../services/user/user.service';

@Injectable()
export class AppHttpInterceptor implements HttpInterceptor {
	constructor(private router: Router, private userService: UserService) {}

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		const cloned = req.clone({
			headers: req.headers.set('Authorization', `Bearer ${this.userService.token}`)
		});

		return next.handle(cloned).pipe();
	}
}
