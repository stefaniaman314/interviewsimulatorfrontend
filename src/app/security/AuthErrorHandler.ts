import { ErrorHandler, Injector, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../services/user/user.service';

@Injectable()
export class AuthErrorHandler implements ErrorHandler {
	constructor(private injector: Injector, private userService: UserService) {}

	handleError(error) {
		const router = this.injector.get(Router);

		if (error.status === 401 || error.status === 403) {
			this.userService.logout();
			router.navigate([ '/login' ]);
		}

		throw error;
	}
}
