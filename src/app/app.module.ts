import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/account/login/login.component';
import { RegisterComponent } from './components/account/register/register.component';
import { RecoverComponent } from './components/account/recover/recover.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { TimerComponent } from './components/timer/timer.component';
import { HomeComponent } from './components/home/home.component';
import { InterviewComponent } from './components/interview/interview.component';
import { FeedbackComponent } from './components/feedback/feedback.component';
import { NotFoundComponent } from './components/error/not-found/not-found.component';
import { AuthErrorHandler } from './security/AuthErrorHandler';

@NgModule({
	declarations: [
		HomeComponent,
		InterviewComponent,
		AppComponent,
		LoginComponent,
		RegisterComponent,
		RecoverComponent,
		TimerComponent,
		FeedbackComponent,
		NotFoundComponent
	],
	imports: [ BrowserModule, AppRoutingModule, HttpClientModule, FormsModule ],
	providers: [],
	bootstrap: [ AppComponent ]
})
export class AppModule {}
