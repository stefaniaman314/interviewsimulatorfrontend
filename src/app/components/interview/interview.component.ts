import { Component, OnInit } from '@angular/core';
import * as tf from '@tensorflow/tfjs';
import { fetch as fetchPolyfill } from 'whatwg-fetch';
import { TimerComponent } from '../timer/timer.component';
import {
	FPS,
	FACE_CASCADE_FILE_NAME,
	MODEL_PATH,
	FACE_CASCADE_FILE_PATH,
	QUIZ_ENDED_MESSAGE
} from 'src/app/helpers/common/Constants';
import { EmotionService } from 'src/app/services/emotion/emotion.service';
import { QuestionService } from 'src/app/services/question/question.service';
import Helpers from 'src/app/helpers/Helpers';
import { UserService } from 'src/app/services/user/user.service';
import { EmotionMapper, Emotion } from 'src/app/models/EmotionMapper';
import { QuestionEmotionMapper } from 'src/app/models/QuestionEmotionMapper';

declare var cv: any;

@Component({
	selector: 'app-interview',
	templateUrl: './interview.component.html',
	styleUrls: [ './interview.component.css' ]
})
export class InterviewComponent implements OnInit {
	// Current interview
	interviewId: number;

	// Current user username
	currentUsername: string;

	// Current user email
	currentEmail: string;

	// Emotions mapper
	emotionMapper: EmotionMapper;

	// Video feed element
	video: HTMLVideoElement;

	// Current quiz question
	question: string;

	// Current quiz question index
	questionIndex: number;

	// Quiz ended
	quizEnded: boolean;

	// CNN model
	private model: any;
	private modelJsonPath: string = MODEL_PATH;

	// Haar Cascade Classifier
	private classifier: any;
	private faceCascadePath: string = FACE_CASCADE_FILE_PATH;

	constructor(
		private userService: UserService,
		private emotionService: EmotionService,
		private questionService: QuestionService
	) {
		this.questionService.getQuizQuestions();

		this.interviewId = Date.now();

		this.currentUsername = this.userService.username;
		this.currentEmail = this.userService.email;

		this.emotionMapper = new EmotionMapper();

		this.questionIndex = 0;
		this.quizEnded = false;
	}

	set interview(interviewId: number) {
		localStorage.setItem('interviewId', interviewId.toString());
	}

	async ngOnInit(): Promise<void> {
		// load CNN model
		this.loadModel();

		// capture video feed
		this.captureVideo();

		// wait for external dependencies to load
		await Helpers.sleep(1000);

		// load the pre-trained classifier
		this.classifier = new cv.CascadeClassifier();
		await Helpers.createFileFromURL(FACE_CASCADE_FILE_NAME, this.faceCascadePath, () => {
			console.log('Loading classifier...');
			this.classifier.load(FACE_CASCADE_FILE_NAME);
			console.log('Classifer loaded succesfully!');
		});

		// load the first question using question service
		this.question = this.questionService.quizQuestions[this.questionIndex].question;
	}

	async loadModel(): Promise<void> {
		window.fetch = fetchPolyfill;
		console.log('Loading model...');

		try {
			this.model = await tf.loadLayersModel(this.modelJsonPath);
			console.log('Model loaded successfully!');
		} catch (err) {
			console.error(`An error occured while loading the model. See more details: ${err}`);
		}
	}

	startInterview() {
		document.getElementById("startInterview").style.display = "none";
		document.getElementById("interviewContent").style.display = "block";
	}

	captureVideo(): void {
		let video: HTMLVideoElement = document.getElementById('videoInput') as HTMLVideoElement;
		let options = {
			audio: false,
			video: true
		};

		navigator.mediaDevices
			.getUserMedia(options)
			.then((stream: any) => {
				video.srcObject = stream;
				video.play();
			})
			.catch((err: any) => {
				console.log(`An error occurred while capturing the video. See more details: ${err}`);
			});

		this.video = video;
	}

	async processVideo(): Promise<void> {
		// disable start recording button
		(document.getElementById('processVideoButton') as HTMLButtonElement).disabled = true;

		// start the timer
		let timer = new TimerComponent();
		timer.startTimer();

		// process video
		let processVideoInterval = setInterval(async () => {
			await this.process(this.classifier);

			// if time is up
			if (timer.timeLeft === 0) {
				// clear timer
				clearInterval(timer.timerInterval);

				// clear processing
				clearInterval(processVideoInterval);

				// clear canvas overlay
				let canvas = document.getElementById('canvasOverlay') as HTMLCanvasElement;
				const context = canvas.getContext('2d');
				context.clearRect(0, 0, canvas.width, canvas.height);

				// enable start recording button
				(document.getElementById('processVideoButton') as HTMLButtonElement).disabled = false;

				// log emotions to analytics for current question
				this.interview = this.interviewId;

				this.emotionService.addEmotionAnalytics(
					this.interviewId,
					this.currentUsername,
					new QuestionEmotionMapper(this.question, this.emotionMapper)
				);

				// reset emotion maper
				this.emotionMapper.resetEmotionCounter();

				// get next question
				if (this.questionService.quizQuestions.length - 1 === this.questionIndex) {
					alert('Quiz ended!');
					this.quizEnded = true;
					document.getElementById("interviewContent").style.display = "none";
					document.getElementById("endInterview").style.display = "block";
					return;
				} else {
					// alert the user
					alert('Time is up!\nNext question...');
					this.question = this.questionService.quizQuestions[++this.questionIndex].question;
				}
			}
		}, 1000 / FPS);
	}

	async process(classifier: any) {
		this.video.height = this.video.videoHeight;
		this.video.width = this.video.videoWidth;
		let src = new cv.Mat(this.video.height, this.video.width, cv.CV_8UC4);
		let dst = new cv.Mat(this.video.height, this.video.width, cv.CV_8UC1);
		let cap = new cv.VideoCapture(this.video);
		let gray = new cv.Mat();
		let faces = new cv.RectVector();

		try {
			// start processing
			cap.read(src);
			src.copyTo(dst);
			cv.cvtColor(dst, gray, cv.COLOR_RGBA2GRAY, 0);

			// detect faces
			classifier.detectMultiScale(gray, faces, 1.1, 3, 0);

			// for each detected face
			for (let i = 0; i < faces.size(); ++i) {
				let face = faces.get(i);

				// draw a box around the face
				let point1 = new cv.Point(face.x, face.y);
				let point2 = new cv.Point(face.x + face.width, face.y + face.height);
				cv.rectangle(dst, point1, point2, [ 255, 0, 0, 255 ]);

				// extract face region
				let faceROI = gray.roi(face);

				// resize face region to 48x48
				let dimension = new cv.Size(48, 48);
				let resizedFaceROI = new cv.Mat();
				cv.resize(faceROI, resizedFaceROI, dimension);

				// convert face region to float data type and scale it
				let floatFaceROI = new cv.Mat();
				resizedFaceROI.convertTo(floatFaceROI, cv.CV_32F, 1.0 / 255);

				// make predictions
				const width = 48;
				const height = 48;
				const colorChannels = 1;

				let tensor = tf.tensor(resizedFaceROI.data).toFloat().expandDims();
				tensor = tensor.reshape([ tensor.shape[0], width, height, colorChannels ]);

				let result = this.model.predict(tensor);
				let maximumResult = tf.argMax(result, 1);
				let index = maximumResult.dataSync()[0];

				switch (index) {
					case 0:
						cv.imshow('canvasOverlay', dst);
						this.emotionMapper.incrementEmotionCounter(Emotion.Anger);
						Helpers.drawText(Emotion[Emotion.Anger], 'canvasOverlay', face);
						break;
					case 1:
						cv.imshow('canvasOverlay', dst);
						this.emotionMapper.incrementEmotionCounter(Emotion.Disgust);
						Helpers.drawText(Emotion[Emotion.Disgust], 'canvasOverlay', face);
						break;
					case 2:
						cv.imshow('canvasOverlay', dst);
						this.emotionMapper.incrementEmotionCounter(Emotion.Fear);
						Helpers.drawText(Emotion[Emotion.Fear], 'canvasOverlay', face);
						break;
					case 3:
						cv.imshow('canvasOverlay', dst);
						this.emotionMapper.incrementEmotionCounter(Emotion.Happiness);
						Helpers.drawText(Emotion[Emotion.Happiness], 'canvasOverlay', face);
						break;
					case 4:
						cv.imshow('canvasOverlay', dst);
						this.emotionMapper.incrementEmotionCounter(Emotion.Sadness);
						Helpers.drawText(Emotion[Emotion.Sadness], 'canvasOverlay', face);
						break;
					case 5:
						cv.imshow('canvasOverlay', dst);
						this.emotionMapper.incrementEmotionCounter(Emotion.Surprise);
						Helpers.drawText(Emotion[Emotion.Surprise], 'canvasOverlay', face);
						break;
					case 6:
						cv.imshow('canvasOverlay', dst);
						this.emotionMapper.incrementEmotionCounter(Emotion.Neutral);
						Helpers.drawText(Emotion[Emotion.Neutral], 'canvasOverlay', face);
						break;
					default:
						break;
				}

				// clear memory
				src.delete();
				dst.delete();
				gray.delete();
			}
		} catch (err) {
			console.log('An error occured while processing the video. See more details: ', err);
		}
	}

	getNextQuestion(): void {
		if (this.questionService.quizQuestions.length - 1 === this.questionIndex) {
			alert('Quiz ended!');
			this.question = QUIZ_ENDED_MESSAGE;
			this.quizEnded = true;
			return;
		}
		this.question = this.questionService.quizQuestions[++this.questionIndex].question;
	}

	logout() {
		this.userService.logout();
	}
}
