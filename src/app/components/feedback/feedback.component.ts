import { Component, OnInit } from '@angular/core';
import { EmotionService } from 'src/app/services/emotion/emotion.service';
import Helpers from 'src/app/helpers/Helpers';
import { EmotionMapper } from 'src/app/models/EmotionMapper';
import { UserService } from 'src/app/services/user/user.service';
import {
	DEFAULT_DATE_TIME_LENGTH,
	ZERO_PADDING,
	NO_QUESTION,
	NO_EMOTION_PERCENT,
	NO_INTERVIEW_DATE
} from 'src/app/helpers/common/Constants';

@Component({
	selector: 'app-feedback',
	templateUrl: './feedback.component.html',
	styleUrls: [ './feedback.component.css' ]
})
export class FeedbackComponent implements OnInit {
	// Current user username
	currentUsername: string;

	// Error message flag
	error: boolean;

	constructor(private userService: UserService, private emotionService: EmotionService) {
		this.currentUsername = this.userService.username;
		this.error = false;
	}

	get interview() {
		return localStorage.getItem('interviewId');
	}

	async ngOnInit(): Promise<void> {
		// process emotion analytics for current interview
		let currentInterviewRaport = this.processEmotionAnalytics(
			await this.emotionService.getEmotionAnalyticsByInterview(parseFloat(this.interview))
		);

		if (currentInterviewRaport && currentInterviewRaport.size > 0) {
			// generate current interview feedback
			this.generateFeedbackTable(currentInterviewRaport, 'currentInterviewTableBody');
		} else {
			this.error = true;
		}

		// generate interviews history
		this.generateInterviewHistory();
	}

	processEmotionAnalytics(analytics: any): Map<string, Map<string, number>> | undefined {
		// Emotions raport map: Map<question, Map<emotion, percent>>
		let emotionsRaportMap: Map<string, Map<string, number>> = new Map();
		let emotionsInnerMap: Map<string, number>;

		if (analytics && analytics.length > 0) {
			analytics.forEach((element) => {
				emotionsInnerMap = new Map();

				let currentQuestion: string = element.feedback.question;
				let currentEmotions: EmotionMapper = element.feedback.emotions;
				let currentEmotionDetections: number = element.feedback.emotions.emotionsCounter;

				let angerPercent: number = Helpers.calculatePercentage(
					currentEmotions.angerCounter,
					currentEmotionDetections
				);
				let disgustPercent: number = Helpers.calculatePercentage(
					currentEmotions.disgustCounter,
					currentEmotionDetections
				);
				let fearPercent: number = Helpers.calculatePercentage(
					currentEmotions.fearCounter,
					currentEmotionDetections
				);
				let happyPercent: number = Helpers.calculatePercentage(
					currentEmotions.happinessCounter,
					currentEmotionDetections
				);
				let sadnessPercent: number = Helpers.calculatePercentage(
					currentEmotions.sadnessCounter,
					currentEmotionDetections
				);
				let surprisePercent: number = Helpers.calculatePercentage(
					currentEmotions.surpriseCounter,
					currentEmotionDetections
				);
				let neutralPercent: number = Helpers.calculatePercentage(
					currentEmotions.neutralCounter,
					currentEmotionDetections
				);

				emotionsInnerMap.set('Anger', angerPercent);
				emotionsInnerMap.set('Disgust', disgustPercent);
				emotionsInnerMap.set('Fear', fearPercent);
				emotionsInnerMap.set('Happyness', happyPercent);
				emotionsInnerMap.set('Sadness', sadnessPercent);
				emotionsInnerMap.set('Surprise', surprisePercent);
				emotionsInnerMap.set('Neutral', neutralPercent);

				emotionsRaportMap.set(currentQuestion, emotionsInnerMap);
			});

			console.log('Processed emotion analytics successfully!');
			this.error = false;
			return emotionsRaportMap;
		} else {
			console.log('Failed to process emotion analytics!');
			this.error = true;
			return undefined;
		}
	}

	generateFeedbackTable(raportMap: Map<string, Map<string, number>>, tableId: string): void {
		const iterator = raportMap.keys();

		// create table body
		let tableBody = document.createElement('tbody');

		// for each question in raport
		for (let i = 0; i < raportMap.size; i++) {
			// create a new row
			let tableRow = document.createElement('tr');

			// add question cell to row
			let question = iterator.next().value || NO_QUESTION;
			let questionTd = document.createElement('td');
			let questionText = document.createTextNode(question);
			questionTd.appendChild(questionText);
			tableRow.appendChild(questionTd);

			let emotions: Map<string, number> = raportMap.get(question);
			let it = emotions.keys();

			// for each emotion
			for (let j = 0; j < emotions.size; j++) {
				// add emotion percent cell to row
				let emotion = it.next().value;
				let emotionTd = document.createElement('td');
				let emotionPercent = emotions.get(emotion) !== undefined ? `${emotions.get(emotion)}%` : NO_EMOTION_PERCENT;
				let emotionText = document.createTextNode(emotionPercent);

				emotionTd.appendChild(emotionText);
				tableRow.appendChild(emotionTd);
			}
			tableBody.appendChild(tableRow);
		}

		document.getElementById(tableId).innerHTML = tableBody.innerHTML;
	}

	async getPastInterviews() {
		let analytics = await this.emotionService.getEmotionAnalyticsByUser(this.currentUsername);

		if (analytics && analytics.length > 0) {
			let interviewSet: Set<number> = new Set();

			analytics.forEach((element) => {
				interviewSet.add(element.interviewId);
			});

			return interviewSet;
		}
		return undefined;
	}

	async generateInterviewHistory() {
		// get past interviews history
		let pastInterviews = await this.getPastInterviews();

		if (pastInterviews && pastInterviews.size > 0) {
			let currentYear: string,
				currentMonth: string,
				currentDay: string,
				currentHour: string,
				currentMinute: string;

			// for each past interview
			pastInterviews.forEach(async (interview) => {
				// add a button for each interview
				let interviewButton = document.createElement('button');
				interviewButton.className = 'btn btn-dark';
				interviewButton.setAttribute('data-toggle', 'collapse');
				interviewButton.setAttribute('data-target', `#${interview}`);
				interviewButton.setAttribute('aria-expanded', 'false');
				interviewButton.setAttribute('aria-controls', interview.toString());
				interviewButton.setAttribute('style', 'margin: 10px;');

				// populate button text with the interview date
				let interviewDate = new Date(interview);
				currentYear = interviewDate.getFullYear().toString();
				currentMonth = (interviewDate.getMonth() + 1)
					.toString()
					.padStart(DEFAULT_DATE_TIME_LENGTH, ZERO_PADDING);
				currentDay = interviewDate.getDate().toString().padStart(DEFAULT_DATE_TIME_LENGTH, ZERO_PADDING);
				currentHour = interviewDate.getHours().toString().padStart(DEFAULT_DATE_TIME_LENGTH, ZERO_PADDING);
				currentMinute = interviewDate.getMinutes().toString().padStart(DEFAULT_DATE_TIME_LENGTH, ZERO_PADDING);

				let interviewText =
					currentDay && currentMonth && currentYear && currentHour && currentMinute
						? `${currentDay}-${currentMonth}-${currentYear} ${currentHour}:${currentMinute}`
						: NO_INTERVIEW_DATE;

				let interviewButtonText = document.createTextNode(interviewText);

				interviewButton.appendChild(interviewButtonText);
				document.getElementById('history').appendChild(interviewButton);

				// create div element to collapse for that interview
				let div = document.createElement('div');
				div.className = 'collapse';
				div.id = interview.toString();

				// create feedback table
				let table = document.createElement('table');
				table.className = 'table';

				// create table head
				let thead = document.createElement('thead');
				thead.className = 'thead-dark';

				// create table row
				let tr = document.createElement('tr');

				// create table head data
				let th1 = document.createElement('th');
				th1.scope = 'col';
				th1.textContent = 'Question';

				let th2 = document.createElement('th');
				th2.scope = 'col';
				th2.textContent = 'Anger';

				let th3 = document.createElement('th');
				th3.scope = 'col';
				th3.textContent = 'Disgust';

				let th4 = document.createElement('th');
				th4.scope = 'col';
				th4.textContent = 'Fear';

				let th5 = document.createElement('th');
				th5.scope = 'col';
				th5.textContent = 'Happiness';

				let th6 = document.createElement('th');
				th6.scope = 'col';
				th6.textContent = 'Sadness';

				let th7 = document.createElement('th');
				th7.scope = 'col';
				th7.textContent = 'Surprise';

				let th8 = document.createElement('th');
				th8.scope = 'col';
				th8.textContent = 'Neutral';

				// add data to tr
				tr.appendChild(th1);
				tr.appendChild(th2);
				tr.appendChild(th3);
				tr.appendChild(th4);
				tr.appendChild(th5);
				tr.appendChild(th6);
				tr.appendChild(th7);
				tr.appendChild(th8);

				// add tr to thead
				thead.appendChild(tr);

				// create tbody
				let tBody = document.createElement('tbody');
				tBody.id = `${interview}TableBody`;

				// add thead and tbody to table
				table.appendChild(thead);
				table.appendChild(tBody);

				// add table to div
				div.appendChild(table);

				// add div to history
				document.getElementById('history').appendChild(div);

				// get analytics for each interview
				let currentAnalytics = await this.emotionService.getEmotionAnalyticsByInterview(interview);

				if (currentAnalytics && currentAnalytics.length > 0) {
					// get feedback data for current interview
					let currentRaport = this.processEmotionAnalytics(currentAnalytics);

					if (currentRaport && currentRaport.size > 0) {
						// populate table body with feedback data
						this.generateFeedbackTable(currentRaport, `${interview}TableBody`);
					} else {
						console.log('Failed to process emotion analytics!');
						this.error = true;
					}
				} else {
					console.log('Failed to retrieve emotion analytics!');
					this.error = true;
				}
			});
		}
	}

	logout() {
		this.userService.logout();
	}
}
