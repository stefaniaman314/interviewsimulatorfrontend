import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../base/base.component';
import { UserService } from 'src/app/services/user/user.service';
import { Router } from '@angular/router';
import { QuestionService } from 'src/app/services/question/question.service';
import Question from 'src/app/models/Question';
import Helpers from 'src/app/helpers/Helpers';

@Component({
	selector: 'app-recover',
	templateUrl: './recover.component.html',
	styleUrls: [ './recover.component.css' ]
})
export class RecoverComponent extends BaseComponent implements OnInit {
	newPassword: string;

	constructor(userService: UserService, router: Router, public questionService: QuestionService) {
		super(userService, router);

		this.questionService.getSecurityQuestions();
	}

	async ngOnInit() {
		if (this.getUserService.logInStatus === 'true') {
			this.getRouter.navigate([ '/' ]);
		}
	}

	async getStatus(): Promise<boolean> {
		return await this.getUserService.recover(
			this.email.toLowerCase(),
			this.securityQuestion,
			this.securityQuestionAnswer,
			this.newPassword
		);
	}

	async onClickRecover(): Promise<void> {
		let status = await this.getStatus();
		this.onClick(status, '/login');
	}
}
