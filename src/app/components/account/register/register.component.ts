import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../base/base.component';
import { UserService } from 'src/app/services/user/user.service';
import { Router } from '@angular/router';
import { QuestionService } from 'src/app/services/question/question.service';
import Question from 'src/app/models/Question';

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: [ './register.component.css' ]
})
export class RegisterComponent extends BaseComponent implements OnInit {
	constructor(userService: UserService, router: Router, public questionService: QuestionService) {
		super(userService, router);

		this.questionService.getSecurityQuestions();
	}

	ngOnInit() {
		if (this.getUserService.logInStatus === 'true') {
			this.getRouter.navigate([ '/' ]);
		}
	}

	async getStatus(): Promise<boolean> {
		return await this.getUserService.register(
			this.username,
			this.password,
			this.email.toLowerCase(),
			this.securityQuestion,
			this.securityQuestionAnswer
		);
	}

	async onClickRegister(): Promise<void> {
		let status = await this.getStatus();
		this.onClick(status, '/login');
	}
}
