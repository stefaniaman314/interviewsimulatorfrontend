import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user/user.service';
import validator from 'validator';
import { QuestionService } from 'src/app/services/question/question.service';

export abstract class BaseComponent implements OnInit {
	username: string;
	email: string;
	password: string;
	securityQuestion: string;
	securityQuestionAnswer: string;

	isEmailError: boolean = false;

	successShown: boolean = false;
	errorShown: boolean = false;

	constructor(private userService: UserService, private router: Router) {}

	ngOnInit() {}

	public get getUserService(): UserService {
		return this.userService;
	}

	public get getRouter(): Router {
		return this.router;
	}

	/**
   * Function that returns the success/failure status after the operation performed.
   * @returns a boolean value. True if the operation was successful, false otherwise.
   */
	abstract async getStatus(): Promise<boolean>;

	/**
   * Function that is executed on button clicked.
   * Redirects the user to the specified page if the operation was successful,
   * sends the errors to the UI otherwise.
   * @param isSuccess
   */
	onClick(isSuccess: boolean, redirect: string): void {
		this.validateEmail(this.email);

		if (isSuccess) {
			this.successShown = true;
			setTimeout(() => {
				this.router.navigate([ redirect ]);
			}, 1000);
		} else {
			this.errorShown = true;
			setTimeout(() => {
				this.errorShown = false;
			}, 1500);
		}
	}

	/**
   * Checks if the email has the correct format and sends the errors to the UI if necessary.
   * @param email
   */
	validateEmail(email: string): void {
		if (validator.isEmail(email)) {
			this.isEmailError = false;
		} else {
			this.isEmailError = true;
		}
	}
}
