import { Component, OnInit, NgModule } from '@angular/core';
import { BaseComponent } from '../base/base.component';
import { UserService } from 'src/app/services/user/user.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: [ './login.component.css' ]
})
export class LoginComponent extends BaseComponent {
	constructor(userService: UserService, router: Router) {
		super(userService, router);
	}

	ngOnInit() {
		if (this.getUserService.logInStatus === 'true') {
			this.getRouter.navigate([ '/' ]);
		}
	}

	async getStatus(): Promise<boolean> {
		return await this.getUserService.login(this.email.toLowerCase(), this.password);
	}

	async onClickLogin(): Promise<void> {
		let status = await this.getStatus();
		this.onClick(status, '/interview');
	}
}
