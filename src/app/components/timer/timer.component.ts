import { Component, OnInit } from '@angular/core';
import { TIME_LIMIT, COLOR_CODES, FULL_DASH_ARRAY } from 'src/app/helpers/common/Constants';

@Component({
	selector: 'app-timer',
	templateUrl: './timer.component.html',
	styleUrls: [ './timer.component.css' ]
})
export class TimerComponent implements OnInit {
	timePassed: number;
	timeLeft: number;
	timerInterval: any;
	remainingPathColor: string;

	constructor() {
		this.timePassed = 0;
		this.timeLeft = TIME_LIMIT;
		this.timerInterval = null;
		this.remainingPathColor = COLOR_CODES.info.color;
	}

	ngOnInit(): void {}

	startTimer() {
		this.timerInterval = setInterval(() => {
			this.timePassed = this.timePassed += 1;
			this.timeLeft = TIME_LIMIT - this.timePassed;
			document.getElementById('base-timer-label').innerHTML = this.formatTime(this.timeLeft);
			this.setCircleDasharray();
			//this.setRemainingPathColor(this.timeLeft);

			// if (this.timeLeft === 0) {
			// 	this.onTimesUp();
			// }
		}, 1000);
	}

	onTimesUp() {
		clearInterval(this.timerInterval);
	}

	formatTime(time: number) {
		const minutes: number = Math.floor(time / 60);
		let seconds: number = time % 60;

		if (seconds < 10) {
			return `${minutes}:0${seconds}`;
		}

		return `${minutes}:${seconds}`;
	}

	setRemainingPathColor(timeLeft: number) {
		const { alert, warning, info } = COLOR_CODES;

		if (timeLeft <= alert.threshold) {
			document.getElementById('base-timer-path-remaining').classList.remove(warning.color);
			document.getElementById('base-timer-path-remaining').classList.add(alert.color);
		} else if (timeLeft <= warning.threshold) {
			document.getElementById('base-timer-path-remaining').classList.remove(info.color);
			document.getElementById('base-timer-path-remaining').classList.add(warning.color);
		}
	}

	calculateTimeFraction() {
		const rawTimeFraction = this.timeLeft / TIME_LIMIT;
		return rawTimeFraction - 1 / TIME_LIMIT * (1 - rawTimeFraction);
	}

	setCircleDasharray() {
		const circleDasharray = `${(this.calculateTimeFraction() * FULL_DASH_ARRAY).toFixed(0)} 283`;
		document.getElementById('base-timer-path-remaining').setAttribute('stroke-dasharray', circleDasharray);
	}
}
