import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user/user.service';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: [ './home.component.css' ]
})
export class HomeComponent implements OnInit {
	logInStatus: boolean;

	constructor(private userService: UserService) {
		if (this.userService.logInStatus === 'true') {
			this.logInStatus = true;
		} else {
			this.logInStatus = false;
		}
	}

	ngOnInit(): void {}

	logout() {
		this.userService.logout();
	}
}
