export default class Question {
	question: string;

	constructor(question: string) {
		this.question = question;
	}
}
