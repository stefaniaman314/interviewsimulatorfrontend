export default class EmotionFrequency {
	emotion: string;
	frequency: number;

	constructor(emotion: string, frequency: number) {
		this.emotion = emotion;
		this.frequency = frequency;
	}
}
