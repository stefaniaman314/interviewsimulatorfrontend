export default class User {
	username: string;
	email: string;
	password: string;
	securityQuestion: string;
	securityQuestionAnswer: string;

	constructor(username: string, email: string) {
		this.username = username;
		this.email = email;
	}
}
