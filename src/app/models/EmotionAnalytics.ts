import EmotionFrequency from './EmotionFrequency';

export default class EmotionAnalytics {
	question: string;
	emotions: Array<EmotionFrequency>;
}
