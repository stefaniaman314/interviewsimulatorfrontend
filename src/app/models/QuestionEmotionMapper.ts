import { EmotionMapper } from './EmotionMapper';

export class QuestionEmotionMapper {
	question: string;
	emotions: EmotionMapper;

	constructor(question: string, emotions: EmotionMapper) {
		this.question = question;
		this.emotions = emotions;
	}
}
