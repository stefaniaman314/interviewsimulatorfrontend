// Environment variables
export const BASE_URL: string = 'http://localhost:8080';
export const API_URL: string = 'http://localhost:8080';
export const TOKEN_KEY: string = 'JWT';

// Timer design
export const FULL_DASH_ARRAY: number = 283;
export const WARNING_THRESHOLD: number = 10;
export const ALERT_THRESHOLD: number = 5;
export const COLOR_CODES = {
	info: {
		color: 'green'
	},
	warning: {
		color: 'orange',
		threshold: WARNING_THRESHOLD
	},
	alert: {
		color: 'red',
		threshold: ALERT_THRESHOLD
	}
};

// Classifier
export const FACE_CASCADE_CLASSIFIER_URL: string =
	'https://raw.githubusercontent.com/opencv/opencv/master/data/haarcascades_cuda/haarcascade_frontalface_default.xml';
export const FACE_CASCADE_FILE_NAME: string = 'haarcascade_frontalface_default.xml';
export const FACE_CASCADE_FILE_PATH: string = '../../../assets/haarcascade_frontalface_default.xml';

// CNN Model
export const MODEL_PATH = '../../../assets/model2/model.json';

// Common
export const QUIZ_ENDED_MESSAGE: string = 'Quiz ended! Check your results below.';
export const TIME_LIMIT: number = 10;
export const FPS: number = 60;
export const DEFAULT_EMOTION_COUNT: number = 0;
export const HOUR_IN_MILLISECONDS: number = 3600000;
export const MINUTE_IN_MILLISECONDS: number = 60000;
export const DEFAULT_DATE_TIME_LENGTH: number = 2;
export const ZERO_PADDING: string = '0';
export const NO_QUESTION = 'Question could not be found';
export const NO_EMOTION_PERCENT = 'Emotion percent could not be calculated';
export const NO_INTERVIEW_DATE = 'Interview could not be found';