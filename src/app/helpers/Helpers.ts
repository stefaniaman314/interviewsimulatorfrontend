import axios from 'axios';

declare var cv: any;

export default class Helpers {
	/**
     * Wait the specified duration,
     * @param ms 
     */
	static sleep(ms: number) {
		return new Promise((resolve) => setTimeout(resolve, ms));
	}

	/**
     * Creates a file with the specified name from the specified url. 
     * @param file 
     * @param url 
     * @param cb 
     */
	static async createFileFromURL(file: any, url: any, cb: any) {
		await axios
			.get(url)
			.then((resp) => {
				cv.FS_createDataFile('/', file, resp.data, true, false, false);
				cb();
			})
			.catch((err) => {
				console.log(`An error occured while creating the file from URL. See more details: ${err}`);
			});
	}

	/**
	 * Draw the specified text o the specified canvas.
	 * @param name 
	 * @param canvasName 
	 * @param face 
	 */
	static drawText(name: string, canvasName: string, face: any) {
		let canvas = document.getElementById(canvasName) as HTMLCanvasElement;
		let ctx = canvas.getContext('2d');
		ctx.font = '60px Monospace';
		ctx.fillStyle = 'red';
		ctx.textAlign = 'center';
		ctx.fillText(name, face.x, face.y);
	}

	static calculatePercentage(emotionCounter: number, detections: number) {
		return Math.round((100 * emotionCounter / detections + Number.EPSILON) * 100) / 100;
	}
}
