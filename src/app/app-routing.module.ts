import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/account/login/login.component';
import { RegisterComponent } from './components/account/register/register.component';
import { RecoverComponent } from './components/account/recover/recover.component';
import { HomeComponent } from './components/home/home.component';
import { InterviewComponent } from './components/interview/interview.component';
import { FeedbackComponent } from './components/feedback/feedback.component';
import { NotFoundComponent } from './components/error/not-found/not-found.component';
import { AuthGuardService } from './services/auth/auth-guard.service';

const routes: Routes = [
	{ path: '', component: HomeComponent },
	{ path: 'interview', component: InterviewComponent, canActivate: [ AuthGuardService ] },
	{ path: 'feedback', component: FeedbackComponent, canActivate: [ AuthGuardService ] },
	{ path: 'login', component: LoginComponent },
	{ path: 'register', component: RegisterComponent },
	{ path: 'recover', component: RecoverComponent },
	{ path: '**', component: NotFoundComponent }
];

@NgModule({
	imports: [ RouterModule.forRoot(routes) ],
	exports: [ RouterModule ]
})
export class AppRoutingModule {}
