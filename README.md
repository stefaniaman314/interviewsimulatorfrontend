# Interview Simulator - partea de client a aplicației
## Cuprins
* [Scurtă descriere](#descriere)
* [Dependențe](#dependențe)
* [Instalare](#instalare)
* [Rularea aplicației](#requirements)

### Scurtă descriere ###
Proiectul de față reprezintă partea de client a aplicației de recunoaștere a emoțiilor faciale în simularea unui interviu. Este o aplicație Angular construită pe ultima versiune stabilă a framework-ului, respectiv Angular 9.0. 

În continuare sunt prezentați pașii care trebuie urmați pentru rularea aplicației.

### Dependențe ###

Pentru ca aplicația să ruleze este necesară instalarea platformei [node.js](http://nodejs.org), împreună cu [npm](http://npmjs.com) (node package manager).

De asemenea, este necesară instalarea `Angular CLI` global. Acest lucru se poate face rulând următoarea comandă într-o consolă (cmd sau PowerShell pentru Windows):

```SH
$ npm install -g @angular/cli
```

### Instalare ###
Aplicația va instala dependențele specificate în fișierul `package.json` prin executarea următoarei comenzi, în directorul sursă al proiectului:

```SH
$ npm install
```
### Rularea aplicației ###
Pentru a rula aplicația se poate executa comanda:
```SH
$ npm start
```
Aplicația mai poate fi pornită prin comanda urmăroare, care va și deschide aplicația în browser-ul default:

```SH
$ ng serve --open
```

Odată pornită, aplicația poate fi utilizată direct în browser.

